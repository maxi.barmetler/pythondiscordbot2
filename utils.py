from typing import *

class default_dict(dict):

    def __init__(self, iterable, **kwargs) -> None:
        super().__init__(iterable, **kwargs)
        self.default = kwargs.get('default', None)

    def __getitem__(self, arg):
        return super().get(arg, self.default)