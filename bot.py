import argparse
import os
import sys

import discord
from discord.ext import commands as discord_commands
from discord.ext import tasks

import commands
import config
import database
import jobs
import message_utils

client = discord.Client()


@tasks.loop(seconds=0.6)
async def job_updater():
    await jobs.update()


@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    job_updater.start()


@client.event
async def on_message(message: discord.Message):
    if message.author == client.user:
        return

    if not message.content.startswith(config.prefix):
        # special stuff
        return

    channel: discord.TextChannel = message.channel
    author: discord.Member = message.author
    guild: discord.Guild = message.guild

    permission_level = 0

    if author.id == config.maintainer_id:
        permission_level = 10

    for role in config.permission_levels:
        if role in [r.name for r in author.roles]:
            permission_level = max(
                permission_level, config.permission_levels[role])

    context = {
        'guild': guild,
        'channel': channel,
        'author': author,
        'message': message
    }

    if config.maintenance_mode and permission_level < 10:
        permission_level = -1

    result: str = await commands.execute(
        message.content[len(config.prefix):], permission_level=permission_level, context=context)
    if result:
        return await message_utils.reply_msg(message, result)


if __name__ == "__main__":

    parser = argparse.ArgumentParser('Python Discord Bot V2')
    parser.add_argument('-t', '--token', dest='token', type=str, metavar='PATH',
                        default="/run/secrets/discord-token",
                        help='Specify path to Bot token')
    parser.add_argument('-m', '--mongo', dest='mongo', type=str, metavar='PATH',
                        default='/run/secrets/mongo-uri',
                        help='Specify path to Mongo URI')
    args = parser.parse_args()

    if not (token := os.getenv('DISCORD_TOKEN')):
        token = (open(args.token, 'r') if args.token !=
                 '-' else sys.stdin).read().strip(' \n')
    print(f'Token: {token}')
    if not (mongo_uri := os.getenv('MONGO_URI')):
        mongo_uri = (open(args.mongo, 'r') if args.mongo !=
                     '-' else sys.stdin).read().strip(' \n')
    database.connection = database.Connection(URI=mongo_uri)
    client.run(token)
