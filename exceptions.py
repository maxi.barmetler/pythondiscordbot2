class PermissionLevelError(Exception):

    def __init__(self, required, actual):
        self.required = required
        self.actual = actual

    def get_message(self):
        return f'Permission Level {self.required} required! ({self.actual})'


def check_permission(required, actual):
    if required > actual:
        raise PermissionLevelError(required, actual)


class NotInitializedError(Exception):

    def __init__(self, *args):
        super().__init__("Not Initialized: ", args)


class ArgumentError(Exception):

    pass
