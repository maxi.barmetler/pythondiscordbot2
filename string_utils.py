import math
import re
import string
from typing import *


def indent(text: Text, spaces: int) -> Text:
    return '\n'.join([' ' * spaces + line for line in text.split('\n')])


def indented_replace(text: Text, old: Text, new: Text) -> Text:
    sections = re.split(f'{re.escape(old)}', text)
    for i in range(len(sections) - 1):
        m = re.search(r' +$', sections[i])
        indentation = 0
        if m:
            start, end = m.span()
            indentation = (end - start)
        sections[i] += indent(new, indentation).lstrip()
    return ''.join(sections)


def indented_replace_dict(text: Text, d: Dict[Text, Text], max_depth: int = 4) -> Text:
    if not max_depth:
        return text

    for key in sorted(d, key=lambda _x: len(_x), reverse=True):
        if key in text:
            new = indented_replace_dict(d[key], d, max_depth - 1)
            text = indented_replace(text, key, new)

    return text


def exctract_code(body: Text) -> Text:
    indices = [m.start() for m in re.finditer('```', body)]
    if len(indices) >= 2:
        code = body[indices[0]+3:indices[1]]
        if code.startswith('python'):
            code = code[len('python'):]
        if code.startswith('py'):
            code = code[len('py'):]
        return code.strip(' \n')
    else:
        return body


def columns(words: List[Text], min_tab_size: int = 16, min_gap: int = 4, quantisation: int = 4, max_width: int = 48):
    longest = max([len(x) for x in words])
    tabsize = max((longest + quantisation + min_gap - 1) //
                  quantisation * quantisation, min_tab_size)
    columnCount = (max_width + (tabsize - longest)) // tabsize
    rowCount = math.ceil(len(words) / columnCount)
    words = words + [''] * (columnCount * rowCount - len(words))
    words = [word + ' ' * (tabsize - len(word)) for word in words]
    return '\n'.join([
        ''.join([
            words[x * rowCount + y]
            for x in range(columnCount)
        ]).strip()
        for y in range(rowCount)
    ])