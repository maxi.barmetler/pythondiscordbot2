import pymongo
from exceptions import *

class Connection:

    def __init__(self, URI: str):
        self.URI = URI
        self.client = pymongo.MongoClient(host=URI)
        self.database = self.client['main']
        self.links = self.database['links']
        self.macros = self.database['macros']

connection: Connection = None

def check_initialized():
    global connection
    if not connection:
        raise NotInitializedError('Database Connection')