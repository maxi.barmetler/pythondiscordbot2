from typing import *
import pymongo
import database
from exceptions import *


def get(guild_id: Text, channel_id: Text) -> List[Text]:
    database.check_initialized()
    filt = {'guild': guild_id, 'channel': channel_id}
    result = database.connection.links.find_one(filt)
    if result:
        return result.get('lines', [])
    return []


def add(guild_id: Text, channel_id: Text, links: Iterable[Text], index: int = -1) -> bool:
    database.check_initialized()

    if not links:
        return False

    filt = {'guild': guild_id, 'channel': channel_id}
    change = {'$each': links}
    if index >= 0:
        change['$position'] = index
    update = {'$push': {'lines': change}}
    database.connection.links.update_one(
        filter=filt, update=update, upsert=True)
    return True


def remove_all(guild_id: Text, channel_id: Text) -> bool:
    database.check_initialized()

    filt = {'guild': guild_id, 'channel': channel_id}
    return database.connection.links.delete_many(filt).deleted_count != 0


def remove(guild_id: Text, channel_id: Text, indices: List[int]) -> bool:
    database.check_initialized()

    lines = get(guild_id, channel_id)
    remove_all(guild_id, channel_id)
    newlines = [line for i, line in enumerate(lines) if i not in indices]
    add(guild_id, channel_id, newlines)
    return len(lines) - len(newlines)

