# Python Discord Bot V2

[![pipeline status](https://gitlab.com/maxi.barmetler/pythondiscordbot2/badges/master/pipeline.svg)](https://gitlab.com/maxi.barmetler/pythondiscordbot2/commits/master)

Version 2 of Python Discord Bot, better than ever! (Not yet, have some god-damn faith Arthur)

### Commands:

```
man:            List commands or show Usage of specified command
info:           Show Info about the Bot!
maintenance:    Toggle maintenance mode on or off or query current state.
permission:     Get your permission level.
links:          Manage Links for Channel!
exec:           Execute a Python-script
def:            Manage Macros for this Server!
```

[Wiki](../../wikis/home)  
[Examples](../../wikis/examples)