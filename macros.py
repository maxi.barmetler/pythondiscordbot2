from typing import *
import pymongo
import database
import itertools
from exceptions import *


def get(guild_id: Text, name: Text) -> Text:
    database.check_initialized()

    filt = {'guild': guild_id, 'name': name}
    result = database.connection.macros.find_one(filt)
    return result and result.get('body')


def get_dict(guild_id: Text) -> Dict[Text, Text]:
    database.check_initialized()

    filt = {'guild': guild_id}
    projection = {'name': 1, 'body': 1}
    result = database.connection.macros.find(filter=filt, projection=projection)
    return {'@' + element['name']: element['body'] for element in result}


def get_names(guild_id: Text, start: int = 0, stop: int = 16) -> List[Text]:
    database.check_initialized()

    filt = {'guild': guild_id}
    projection = {'name': 1}
    result = itertools.islice(database.connection.macros.find(
        filter=filt, projection=projection, sort=[('name', 1)]), start, stop)
    names = [element['name'] for element in result]
    return names


def update(guild_id: Text, name: Text, body: Text):
    database.check_initialized()

    filt = {'guild': guild_id, 'name': name}
    update = {'$set': {'guild': guild_id, 'name': name, 'body': body}}
    result = database.connection.macros.update_one(filt, update, upsert=True)
    return result.modified_count or result.raw_result.get('upserted')


def remove(guild_id: Text, name: Union[Text, List[Text]]):
    database.check_initialized()

    if isinstance(name, list):
        filt = {'guild': guild_id, 'name': {'$in': name}}
    else:
        filt = {'guild': guild_id, 'name': name}
    return database.connection.macros.delete_many(filt).deleted_count
