from io import StringIO
import sys
import random
import time
import argparse
import unittest
import commands
import string_utils
import secure_utils
import database
import links
import macros


class TestCommands(unittest.TestCase):

    async def test_missing(self):
        self.assertEqual(None, await commands.execute('foo bar'))

    async def test_info(self):
        self.assertEqual(commands.info.info(), await commands.execute('info'))

    async def test_man(self):
        keys_expected = set(commands.handlers.keys())
        keys_actual = set([line.split(':')[0] for line in await commands.execute(
            'man').split('\n') if not line.startswith((' ', '`', '>'))])
        self.assertEqual(keys_expected, keys_actual)


class TestStringUtils(unittest.TestCase):

    def test_indent(self):
        source = (
            'class A:\n'
            '  \n'
            '  def __init__(self, x):\n'
            '    self.x = x\n'
            '  \n'
            '  def get_x(self):\n'
            '    return self.x'
        )
        expected = (
            '    class A:\n'
            '      \n'
            '      def __init__(self, x):\n'
            '        self.x = x\n'
            '      \n'
            '      def get_x(self):\n'
            '        return self.x'
        )
        self.assertEqual(expected, string_utils.indent(source, 4))

    def test_indented_replace(self):
        source = (
            'class A:\n'
            '  \n'
            '  @constructor'
        )
        expected = (
            'class A:\n'
            '  \n'
            '  def __init__(self, x):\n'
            '    self.x = x'
        )
        constructor = (
            'def __init__(self, x):\n'
            '  self.x = x'
        )
        self.assertEqual(expected, string_utils.indented_replace(
            source, '@constructor', constructor))

    def test_indented_replace_dict(self):
        source = (
            'class A:\n'
            '  \n'
            '  @fun\n'
            '  \n'
            '  @fun2'
        )
        expected = (
            'class A:\n'
            '  \n'
            '  def __init__(self, x):\n'
            '    self.x = x\n'
            '  \n'
            '  def get_x(self):\n'
            '    return self.x'
        )
        fun = (
            'def __init__(self, x):\n'
            '  self.x = x'
        )
        fun2 = (
            'def get_x(self):\n'
            '  return self.x'
        )
        self.assertEqual(expected, string_utils.indented_replace_dict(
            source, {'@fun': fun, '@fun2': fun2}, max_depth=1))


class TestSecureUtils(unittest.TestCase):

    def test_exec_1(self):
        code = 'asd = 1 + 2\nx = f"val={asd}"\nprint("x: " + x)\nreturn asd'
        stream = StringIO()
        ret, flag = secure_utils.exec_secure(code=code, file=stream)
        output = stream.getvalue()
        self.assertEqual('x: val=3\n', output)
        self.assertEqual(3, ret)

    def test_eval_1(self):
        code = '[x["name"] for x in [{"name": "Maxi", "age": 69}, {"age": 420, "name": "Raka"}, {"name": "Ruilin"}]]'
        expected = ['Maxi', 'Raka', 'Ruilin']
        self.assertEqual(expected, secure_utils.eval_secure(code))


class TestLinks(unittest.TestCase):

    def test_01_setup(self):
        database.connection.links.delete_many(
            {'guild': 'guild', 'channel': 'channel'})
            

    def test_02_insert(self):
        lines = ['Line1', 'Line2']
        links.add('guild', 'channel', lines)
        actual = database.connection.links.find_one(
            {'guild': 'guild', 'channel': 'channel'})['lines']
        self.assertEqual(lines, actual)

    def test_03_get(self):
        expected = database.connection.links.find_one(
            {'guild': 'guild', 'channel': 'channel'})['lines']
        actual = links.get('guild', 'channel')
        self.assertEqual(expected, actual)

    def test_04_insert(self):
        lines = ['Line1.2', 'Line1.2']
        links.add('guild', 'channel', lines, index=1)
        actual = database.connection.links.find_one(
            {'guild': 'guild', 'channel': 'channel'})['lines']
        self.assertEqual(['Line1'] + lines + ['Line2'], actual)

    def test_05_remove_all(self):
        links.remove_all('guild', 'channel')
        self.assertEqual([], links.get('guild', 'channel'))

    def test_06_insert(self):
        lines = [f'Line{i}' for i in range(16)]
        links.add('guild', 'channel', lines)
        actual = database.connection.links.find_one(
            {'guild': 'guild', 'channel': 'channel'})['lines']
        self.assertEqual(lines, actual)

    def test_07_remove(self):
        lines = [f'Line{i}' for i in range(16)]
        indices = random.choices(range(16), k=8)
        expected = [line for i, line in enumerate(lines) if i not in indices]
        links.remove('guild', 'channel', indices)
        actual = links.get('guild', 'channel')
        self.assertEqual(expected, actual)


class TestMacros(unittest.TestCase):

    def test_01_setup(self):
        database.connection.macros.delete_many({'guild': 'guild'})

    def test_02_get_names(self):
        documents = [{'guild': 'guild', 'name': f'fun{i:02}', 'body': f'This is numbero {i}'} for i in range(16)]
        shuffled = documents.copy()
        random.shuffle(shuffled)
        database.connection.macros.insert_many(shuffled)
        actual = macros.get_names('guild', 0, 16)
        self.assertEqual([element['name'] for element in documents], actual)
    
    def test_03_get_nothing(self):
        self.assertFalse(macros.get('guild', 'fun16'))
    
    def test_04_get(self):
        self.assertEqual('This is numbero 9', macros.get('guild', 'fun09'))
    
    def test_05_update_existing(self):
        macros.update('guild', 'fun10', 'Test Boi')
        self.assertEqual('Test Boi', macros.get('guild', 'fun10'))
    
    def test_06_update_new(self):
        macros.update('guild', 'fun100', 'Test Boi 100')
        self.assertEqual('Test Boi 100', macros.get('guild', 'fun100'))
    
    def test_07_setup(self):
        database.connection.macros.delete_many({'guild': 'guild'})
        documents = [{'guild': 'guild', 'name': f'fun{i:02}', 'body': f'This is numbero {i}'} for i in range(16)]
        database.connection.macros.insert_many(documents)
    
    def test_08_dict(self):
        expected = {f'@fun{i:02}': f'This is numbero {i}' for i in range(16)}
        actual = macros.get_dict('guild')
        self.assertEqual(expected, actual)
    
    def test_09_remove_single(self):
        name = 'fun03'
        documents = [f'fun{i:02}' for i in range(16) if i != 3]
        macros.remove('guild', 'fun03')
        self.assertEqual(documents, macros.get_names('guild', 0, 16))
    
    def test_10_remove_multiple(self):
        names = [f'fun{i:02}' for i in [5, 9, 4, 15]]
        documents = [f'fun{i:02}' for i in range(16) if i not in [3, 5, 9, 4, 15]]
        macros.remove('guild', names)
        self.assertEqual(documents, macros.get_names('guild', 0, 16))


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Python Discord Bot V2')
    parser.add_argument('-m', '--mongo', dest='mongo', type=str, metavar='URI',
                        help='Specify Mongo URI')
    parser.add_argument('unittest_args', nargs='*')
    args = parser.parse_args()

    mongo_uri = args.mongo
    print(f'mongo-uri: {mongo_uri}')
    database.connection = database.Connection(URI=mongo_uri)

    unittest.main(argv=(sys.argv[0:1] + args.unittest_args))
