import utils

prefix = '$'
maintainer_id = 363323142137184260
maintenance_mode = False

import_requirements = utils.default_dict({
    '____numpy': 2,
    'random': 2,
    'math': 2,
    'time': 2,
    '____requests': 5
}, default=10)

allowed_hosts = [
    'youtube.com',
    'home.in.tum.de/~barmetle',
    'plain-text-ip.com'
]

permission_levels = {
    'Moderator': 5,
    'Admin': 9
}