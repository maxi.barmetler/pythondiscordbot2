import io

version = open('VERSION', 'r').read().strip(' \n')

def info():
    return(
        '> Python Discord Bot V2\n'
        f'Version `{version}`\n'
        'Made by Maxi\n'
        'Gitlab: https://gitlab.com/maxi.barmetler/pythondiscordbot2'
    )