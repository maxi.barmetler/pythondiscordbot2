import discord

async def reply_msg(message: discord.Message, reply: str, inline=False) -> discord.Message:
    return await message.channel.send(
        f'{message.author.mention}' + ("\n" if not inline else "") + reply
    )