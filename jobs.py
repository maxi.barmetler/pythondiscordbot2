import commands
import sys
import threading
from exceptions import *
from io import StringIO
from typing import *

import discord

import macros
import secure_utils


class Job:

    def __init__(self, channel: discord.TextChannel, author: discord.User,
                 target: Callable, ret: List, *,
                 file: StringIO = sys.stdout,
                 max_lines: int = 16):
        self.started = False
        self.finished = False
        self.thread = threading.Thread(target=target)
        self.author = author
        self.channel = channel
        self.stream = file
        self.message: discord.Message = None
        self.ret = ret
        self.max_lines = max_lines

    async def update_message(self):
        output = '\n'.join(self.stream.getvalue().strip(
            ' \n').split('\n')[-self.max_lines:])
        if not output:
            output = '...'
        ret = self.ret[0]
        flag = self.ret[1] or ''
        await self.message.edit(content=(
            f'{self.author.mention}\n'
            '```\n'
            f'{output}'
            '\n```' +
            (f'Return:```{flag}\n{str(ret)}\n```' if ret else '')
        ))


jobs: List[Job] = []

def enqueue(code: Text,
            guild: discord.Guild,
            channel: discord.TextChannel,
            author: discord.User, *,
            max_depth: int = 4,
            timeout: float = 16.0,
            print_only: bool = False,
            permission_level: int = 0):

    check_permission(2, permission_level)

    print('enqueue')

    ret = [None, None]

    stream = StringIO()
    macros_dict = macros.get_dict(str(guild.id))

    replace_import = permission_level < 10

    def target():
        try:
            ret[0], ret[1] = secure_utils.exec_secure(
                code,
                allowed_imports=secure_utils.get_allowed_imports(
                    permission_level),
                alternate_imports=secure_utils.get_replaced_imports(
                    permission_level),
                replace_import=replace_import,
                macros=macros_dict,
                timeout=timeout,
                print_only=print_only,
                max_depth=max_depth,
                file=stream
            )
        except Exception as e:
            print(e)
            pass

    new_job = Job(channel, author, target, ret, file=stream)
    jobs.append(new_job)


async def update():

    global jobs

    if not jobs:
        return

    # Start new jobs
    for job in jobs:
        if not job.started:
            print('start')
            job.message = await job.channel.send(
                f'{job.author.mention}\n'
                '```\nStarting...\n```'
            )
            job.started = True
            job.thread.start()

    # Update running jobs:
    for job in jobs:
        if job.started and job.thread.is_alive():
            await job.update_message()

    # End finished jobs:
    for job in jobs:
        if job.started and (not job.thread.is_alive()) and (not job.finished):
            await job.update_message()
            job.finished = True

    # Clean up finished jobs:
    jobs = [job for job in jobs if not job.finished]
