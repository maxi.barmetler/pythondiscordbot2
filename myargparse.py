import argparse
import sys

class ArgumentParserError(Exception):

    def __init__(self, message):
        super().__init__(message)
        self.message = message


class ThrowingArgumentParser(argparse.ArgumentParser):

    def __init__(self, **kwargs):
        kwargs['add_help'] = False
        super().__init__(**kwargs)
        self.add_argument('-h', '--help', action='store_true', dest='help',
                          help='Print help and exit')

    def error(self, message):
        raise ArgumentParserError(message)
