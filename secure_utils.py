import builtins
import commands
import math
import random
import re
import sys
from io import StringIO
from typing import *
from typing import TextIO

import numpy
import requests
from func_timeout import FunctionTimedOut, func_timeout

import config
import string_utils

realimport = builtins.__import__
verbose = False


def exec_secure(code: Text, *,
                globals: Dict[Text, Any] = {},
                locals: Dict[Text, Any] = {},
                allowed_imports: Iterable[Text] = [],
                alternate_imports: Dict[Text, Any] = {},
                replace_import: bool = True,
                macros: Dict[Text, Text] = {},
                max_depth: int = 4,
                print_only: bool = False,
                timeout: float = 16.0,
                file: TextIO = sys.stdout):

    globals = globals.copy()
    locals = locals.copy()

    if len(code.strip()) == 0:
        raise Exception('No code supplied!')

    # replace macros
    code = string_utils.indented_replace_dict(code, macros, max_depth)

    # Wrap code in a function, so that we can use return
    # indent code
    wrapper = (
        'def __return_value_function__():\n'
        '  @code\n'
        '\n'
        '__return_value__[0] = __return_value_function__()'
    )

    code = string_utils.indented_replace(wrapper, '@code', code)
    ret = [None]
    globals['__return_value__'] = ret

    if print_only:
        return (code, 'python')

    oldimport = builtins.__import__

    def myimport(name: Text, globals: Optional[Mapping[str, Any]] = ...,
                 locals: Optional[Mapping[str, Any]] = ...,
                 fromlist: Sequence[str] = ...,
                 level: int = ...) -> Any:
        if name in alternate_imports:
            return alternate_imports[name]

        if name not in allowed_imports:
            raise Exception(f'Illegal import: {name}')
            return None

        return oldimport(name, globals=globals, locals=locals, fromlist=fromlist, level=level)

    if replace_import:
        builtins.__import__ = myimport

    def printToFile(*values, **kwargs) -> None:
        kwargs['file'] = file
        print(*values, **kwargs)

    if verbose:
        printToFile(f'Executing:\n[[\n{string_utils.indent(code, 2)}\n]]')

    globals['print'] = printToFile

    try:
        func_timeout(timeout, lambda: exec(code, globals, locals))
    except FunctionTimedOut as e:
        # print('Timed out!')
        printToFile(f'Timed out! (Timeout: {timeout})')
        pass
    except Exception as e:
        printToFile(f'Exception:\n[[\n{string_utils.indent(str(e), 2)}\n]]')
        pass
    except:
        printToFile('Unknown Error!')
        pass

    # Revert
    builtins.__import__ = oldimport

    return (ret[0], None)


def eval_secure(code: str, timeout=16):
    try:
        return func_timeout(timeout, lambda: eval(code, {'random': random, 'numpy': numpy, 'math': math, '__import__': None}))
    except TimeoutError:
        print('Timed out!')
        return None


class secure_requests:

    def get(url: Text, params: Dict[Any, Any] = {}, **kwargs):
        protocol = None
        if '//' in url:
            protocol, url = url.split('://')[:2]
        for i in range(url.count('/')):
            url = re.sub(r'\/[^/]*\/\.\.(\/|$)', '/', url)
        if protocol:
            url = f'{protocol}://{url}'
        if not re.match(
            (
                r'^([a-zA-T])*(\:\/\/)([a-zA-Z0-9]+\.)*' +
                '({})'.format(
                    '|'.join(
                        re.escape(host) for host in config.allowed_hosts
                    )
                )
                + r'(\/.*)?'
            ),
            url
        ):
            raise Exception(f'Illegal url: {url}')
        __old_import__ = builtins.__import__
        builtins.__import__ = realimport
        r = requests.get(url, params=params, **kwargs)
        builtins.__import__ = __old_import__
        return r


def fail_operation(name):
    raise PermissionError(f'Operation not permitted: "{name}"')


class secure_numpy:

    def __getattribute__(self, name):
        if name.startswith('save'):
            fail_operation(name)
        if name in ['tofile']:
            fail_operation(name)
        return getattr(numpy, name)


replaced_imports = {
    'requests': secure_requests,
    'numpy': secure_numpy()
}


def get_allowed_imports(permission_level):
    return [module for module, level in config.import_requirements.items() if level <= permission_level and not module.startswith('____')]


def get_replaced_imports(permission_level):
    return {key: value for key, value in replaced_imports.items() if config.import_requirements[f'____{key}'] <= permission_level and config.import_requirements[key] > permission_level}
