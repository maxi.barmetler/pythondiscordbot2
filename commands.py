import io
import itertools
import math
import re
import shlex
import sys
from exceptions import *
from io import StringIO
from typing import *

import discord

import config
import info
import jobs
import links
import macros
import secure_utils
import string_utils
from myargparse import *


class Handler:

    name = None

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        raise NotImplementedError

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        raise NotImplementedError


class Handler_Man(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0,
                      tab=16) -> str:
        commandargs = args.get('commandargs')
        if not commandargs:
            return (
                '> Commands:\n'
                '```\n' +
                '\n'.join([
                    name + ':' + (
                        (' ' * (tab - len(name) - 1))
                        if tab - len(name) >= 2 else '\n' + ' ' * tab
                    ) + value.parser().description for name, value in handlers.items()
                ])
                + '\n```'
            )
        if commandargs[:5] == ['man'] * 5:
            return 'Jesus Christ, relax man'

        check_permission(1, permission_level)

        global execute
        return await execute(f'{commandargs[0]} --help')

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(
            description='List commands or show Usage of specified command')
        parser.add_argument('commandargs', metavar='CMD', type=str, nargs='*',
                            help='command to get info from')
        return parser


class Handler_Info(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        return info.info()

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(description='Show Info about the Bot!')
        return parser


class Handler_Maintenance(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        check_permission(9, permission_level)
        mode = args['mode'].lower()
        if mode == 'on':
            config.maintenance_mode = True
            return 'Turned maintenance mode on.'
        elif mode == 'off':
            config.maintenance_mode = False
            return 'Turned maintenance mode off.'
        return 'Maintenance mode is currently ' + ('on' if config.maintenance_mode else 'off') + '.'

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(
            description='Toggle maintenance mode on or off or query current state.')
        parser.add_argument('mode', nargs='?', metavar='ON/OFF', type=str, default='',
                            help='Set maintenance mode to `True` ("on") or `False` ("off").')
        return parser


class Handler_Permission(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        return f'Your permission level is: `{permission_level}`'

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(
            description='Get your permission level.')
        return parser


class Handler_Links(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:

        guild_id = str(context['guild'].id)
        channel_id = str(context['channel'].id)
        mode = args['mode']

        positional_tokens = args['positional']
        if positional_tokens and mode == 'list':
            if positional_tokens[0] == 'add':
                mode = 'add'
                positional_tokens = positional_tokens[1:]
            elif positional_tokens[0] == 'remove':
                mode = 'remove'
                positional_tokens = positional_tokens[1:]

        positional = ' '.join(positional_tokens).strip()

        if mode == 'list':
            lines = links.get(guild_id, channel_id)
            if not lines:
                return '> No links for channel {0.mention}!'.format(context['channel'])
            text = '\n'.join(
                [f'`[{i:02d}]` {line}' for i, line in enumerate(lines)])
            return '> Links for channel {0.mention}:\n'.format(context['channel']) + text

        elif mode == 'add':
            index = -1
            if positional:
                try:
                    index = secure_utils.eval_secure(positional)
                except:
                    pass
            if not isinstance(index, int):
                index = -1
            lines = [line.strip() for line in body.split('\n')]
            lines = [line for line in lines if line]
            if not lines:
                return '> No links supplied!'
            links.add(guild_id, channel_id, lines, index)
            return '> Links for channel {0.mention} updated!'.format(context['channel'])

        elif mode == 'remove':
            indices = []
            if positional:
                if positional.lower() == 'all':
                    indices = 'all'
                else:
                    try:
                        evaluated = secure_utils.eval_secure(positional)
                    except:
                        evaluated = []
                    if isinstance(evaluated, int):
                        indices = [evaluated]
                    elif isinstance(evaluated, Iterable):
                        indices = list(evaluated)
            if not indices:
                return '> No indices supplied!'
            if indices == 'all':
                removed = links.remove_all(guild_id, channel_id)
                if removed:
                    return '> All links for channel {0.mention} removed!'.format(context['channel'])
                else:
                    return '> No links removed!'
            else:
                removed = links.remove(guild_id, channel_id, indices)
                if removed:
                    return '> {0} link{1} for channel {2.mention} removed!'.format(removed, 's' if removed != 1 else '', context['channel'])
                else:
                    return '> No links removed!'

        raise NotImplementedError()

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(
            description='Manage Links for Channel!')
        parser.set_defaults(mode='list')
        parser.add_argument('-l', '--list', dest='mode', action='store_const', const='list',
                            help='List links of current channel.')
        parser.add_argument('-a', '--add', dest='mode', action='store_const', const='add',
                            help='Add links from the following lines at specified index.')
        parser.add_argument('-r', '--remove', dest='mode', action='store_const', const='remove',
                            help='Remove links with specified indices.')
        parser.add_argument('positional', metavar='EXPR', nargs='*',
                            help='Additional expression if needed, more in man page.')
        return parser


class Handler_Exec(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        guild: discord.Guild = context['guild']
        channel: discord.TextChannel = context['channel']
        author: discord.User = context['author']
        timeout: float = args['timeout']
        print_only: bool = args['print']

        # Extract code from body
        code = string_utils.exctract_code(body)
        print(f'Executing\n{code}')

        jobs.enqueue(code, guild, channel, author,
                     timeout=timeout, print_only=print_only,
                     permission_level=permission_level)

        print(args.positional)

        return None

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(description='Execute a Python-script')
        parser.add_argument('-t', '--timeout', metavar='SEC', type=float,
                            dest='timeout', default=16.0,
                            help='Stop Execution after SEC seconds.')
        parser.add_argument('-p', '--print', action='store_true', dest='print',
                            help='Print expanded code and exit.')
        parser.add_argument('positional', nargs='...',
                            help='Positional Arguments')
        return parser


class Handler_Def(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> str:
        guild: discord.Guild = context['guild']
        mode = args['mode']

        positional_tokens = args['positional']
        if positional_tokens and mode == 'list':
            if positional_tokens[0] == 'get':
                mode = 'get'
                positional_tokens = positional_tokens[1:]
            elif positional_tokens[0] == 'set':
                mode = 'set'
                positional_tokens = positional_tokens[1:]
            elif positional_tokens[0] == 'remove':
                mode = 'remove'
                positional_tokens = positional_tokens[1:]

        positional = ' '.join(positional_tokens).strip()

        if mode == 'list':
            page_size = args['pagesize']
            tabsize = 16
            maxwidth = 48
            page = 0
            if positional:
                try:
                    page = secure_utils.eval_secure(positional)
                except:
                    pass
                if not isinstance(page, int):
                    page = 0
            names = macros.get_names(
                str(guild.id), start=page_size * page, stop=page_size * (page + 1))
            if not names:
                return '> No macros on page `{0}` of `{1.name}`!'.format(page, guild)
            text = string_utils.columns(
                names,
                min_tab_size=tabsize,
                min_gap=4,
                quantisation=4,
                max_width=maxwidth)
            return (
                f'> Macros (page: `{page}`):\n'
                f'```\n{text}\n```'
            )

        if not positional:
            return '> No name supplied!'

        name = positional.lstrip('@')

        if mode == 'get':
            body = macros.get(str(guild.id), name)
            if not body:
                return (
                    f'> `@{name}` doesn\'t exist!'
                )
            return (
                f'> `@{name}` evaluates to:\n'
                f'```python\n{body}\n```'
            )

        if mode == 'remove':
            # check if name is a list
            if not re.match(r'^[a-zA-Z0-9_]*$', name):
                if re.match(r'^[a-zA-Z0-9_]*( *, *@*[a-zA-Z0-9_]*)*$', name):
                    name = [x.strip().lstrip('@') for x in name.split(',')]
                else:
                    try:
                        ev = secure_utils.eval_secure(name)
                        if isinstance(ev, (str, list)):
                            name = ev
                    except:
                        pass
            if isinstance(name, list):
                # Get existing macros for feedback
                existing = macros.get_names(str(guild.id))
                to_remove = [x for x in name if x in existing]
                if not to_remove:
                    return (
                        '> Nothing to remove!'
                    )
                removed = macros.remove(str(guild.id), to_remove)
                if removed != len(to_remove):
                    return (
                        f'> Unexpected error: removed `{removed}`, but found `{len(to_remove)}`!\n'
                        f'```\n{str(to_remove)}\n```'
                    )
                return (
                    f'> Removed `{removed}` macros:\n'
                    f'```\n{str(to_remove)}\n```'
                )
            else:
                removed = macros.remove(str(guild.id), name)
                if removed:
                    return (
                        f'> Removed `{name}`!'
                    )
                else:
                    return (
                        f'> `@{name}` doesn\'t exist!'
                    )

        if mode == 'set':
            # check if name is valid
            if not re.match(r'^[a-zA-Z0-9_]*$', name):
                return (
                    f'> Invalid name: `{name}`'
                )
            # Extract code from body
            code = string_utils.exctract_code(body)
            updated = macros.update(str(guild.id), name, code)
            if not updated:
                return (
                    f'> `{name}` not updated.\nEither nothing changed, or an unexpected error occurred.'
                )
            return (
                f'> Sucessfully updated `{name}`!'
            )

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(
            description='Manage Macros for this Server!')
        parser.set_defaults(mode='list')
        parser.add_argument('-l', '--list', dest='mode', action='store_const', const='list',
                            help='List all macros.')
        parser.add_argument('-g', '--get', dest='mode', action='store_const', const='get',
                            help='Get value of specified macro.')
        parser.add_argument('-s', '--set', dest='mode', action='store_const', const='set',
                            help='Set value of specified macro.')
        parser.add_argument('-r', '--remove', dest='mode', action='store_const', const='remove',
                            help='Remove specified macro.')
        parser.add_argument('-p', '--pagesize', dest='pagesize', type=int, default=16,
                            help='How many lines to print per page.')
        parser.add_argument('positional', metavar='EXPR', nargs='*',
                            help='Additional expression if needed, more in man page.')
        return parser


class Handler_QR(Handler):

    @staticmethod
    async def execute(args: Dict[Text, Any], body: Text, context: Dict[str, Any] = {}, *,
                      permission_level: int = 0) -> any:
        guild: discord.Guild = context['guild']
        channel: discord.TextChannel = context['channel']
        author: discord.User = context['author']
        message: discord.Message = context['message']
        deleteMessage = bool(args['delete'])
        string = ' '.join(args['words'])
        box_size = int(args['box_size'])
        border = int(args['border'])
        type = args['type']

        if deleteMessage:
            await message.delete()

        import qrcode

        qr = qrcode.QRCode(
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=box_size,
            border=border,
        )
        qr.add_data(string)
        qr.make(fit=True)

        if (type == 'utf8'):
            arr = io.StringIO()
            qr.print_ascii(out=arr, invert=True)
            return f'```{arr.getvalue()}```'
        else:
            img = qr.make_image(fill_color='black', back_color='white')
            arr = io.BytesIO()
            img.save(arr, format='PNG')
            arr.seek(0)
            await channel.send(file=discord.File(arr, 'qrcode.png'))

    @staticmethod
    def parser() -> ThrowingArgumentParser:
        parser = ThrowingArgumentParser(description='Generate qr codes')
        parser.add_argument('-s', '--box-size', type=int, default=10,
                            help='size of each square, default is 10')
        parser.add_argument('-b', '--border', type=int, default=2,
                            help='border around qr code, default is 2')
        parser.add_argument(
            '-d', '--delete', action='store_true', help='Delete your message')
        parser.add_argument(
            '-t', '--type', choices=['utf8', 'png'], default='png')
        parser.add_argument('words', nargs='+', help='String to encode')
        return parser


handlers = {value.name if value.name else name.replace('Handler_', '').lower(): value
            for name, value in globals().items() if name.startswith('Handler_')}


async def execute(message: Text, context: Dict[str, Any] = {}, *,
                  permission_level: int = 0) -> str:
    message = message.replace('\r\n', '\n').replace('\r', '\n').strip(' \n')
    simplified_message = message.replace('\\\n', '').strip(' \n')
    if len(simplified_message) == 0:
        return None
    body = ''
    try:
        body = '\n'.join(list(itertools.dropwhile(
            lambda line: line.endswith('\\'), message.split('\n')))[1:])
    except:
        pass
    command_line = simplified_message.split('\n')[0]
    command_tokens = shlex.split(command_line)
    command = command_tokens[0]
    if command not in handlers:
        return None
    handler: Handler = handlers.get(command)
    parser = handler.parser()
    parser.prog = command
    args = {}
    try:
        args = vars(parser.parse_args(command_tokens[1:]))
    except ArgumentParserError as e:
        file = io.StringIO()
        parser.print_help(file)
        result = file.getvalue().strip(' \n')
        return f'```\n{e.message}\n``````\n{result}\n```'

    if args.get('help', False):
        file = io.StringIO()
        parser.print_help(file)
        result = file.getvalue().strip(' \n')
        return f'```\n{result}\n```'

    try:
        return await handler.execute(args=args, body=body, context=context, permission_level=permission_level)
    except PermissionLevelError as e:
        return f'`{e.get_message()}`'
